FROM python:alpine3.6
RUN apk update && \
    apk add gcc python3-dev musl-dev postgresql-dev postgresql-client bash curl
COPY ./requirements.txt /
RUN pip install -r requirements.txt
COPY . /server/
WORKDIR /server
#RUN python3 manage.py makemigrations && python3 manage.py migrate
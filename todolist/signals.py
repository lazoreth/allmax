def create_admin_group(sender, **kwargs):
    from django.contrib.auth.models import Group
    if not Group.objects.filter(name="Admin").exists():
        admin = Group(name="Admin")
        admin.save()

def create_superuser(sender, **kwargs):
    import os
    from django.contrib.auth.models import User, Group
    if not User.objects.filter(username=os.getenv("DJANGO_SU_NAME")).exists():
        admin = User.objects.create_superuser(
            username=os.getenv("DJANGO_SU_NAME"),
            email=os.getenv("DJANGO_SU_EMAIL"),
            password=os.getenv("DJANGO_SU_PASS")
        )
        admin_group = Group.objects.get(name="Admin")
        admin_group.user_set.add(admin)
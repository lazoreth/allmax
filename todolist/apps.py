from django.apps import AppConfig
from django.db.models.signals import post_migrate


class TodolistConfig(AppConfig):
    name = 'todolist'

    def ready(self):
        from .signals import create_admin_group, create_superuser
        post_migrate.connect(create_admin_group, sender=self)
        post_migrate.connect(create_superuser, sender=self)

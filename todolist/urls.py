"""allmax URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from todolist import views
from django.contrib.auth import views as auth_views
from django.contrib.auth.decorators import login_required


urlpatterns = [
    path('login', auth_views.LoginView.as_view(
        template_name='todolist/login.html'),
        name='login'
        ),
    path('logout', auth_views.logout,
        {'next_page':'login'},
        name='todolist-logout'),
    path('sign-up', views.sign_up, name='sign-up'),
    path('', 
        login_required(
        views.TaskList.as_view(),
        login_url='login'),
        name='tasks'
        ),
    path('detail/<int:pk>/',
        login_required(
        views.TaskView.as_view(),
        login_url='login'),
        name='detail'
        ),
    path('create',
        login_required(
        views.CreateTask.as_view(),
        login_url='login'),
        name='create'
        )
]


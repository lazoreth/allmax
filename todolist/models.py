from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator
from django.contrib.auth.models import User


# Create your models here.
class Task(models.Model):
    def __str__(self):
        return self.title

    STATUSES = (
        ("Waiting", "Waiting"),
        ("In do", "In do"),
        ("Done", "Done"),
        ("Cancelled", "Cancelled")
    )

    PRIORITY_VALUES = zip(range(10), range(10))

    title = models.CharField(max_length=50, verbose_name="Название", null=False)
    description = models.TextField(max_length=1000, verbose_name="Описание", null=True)
    created_at = models.DateTimeField(auto_now_add=True, verbose_name="Дата создания")
    priority = models.IntegerField(
        default=1,
        choices=PRIORITY_VALUES,
        verbose_name="Приоритет"
    )
    status = models.CharField(
        max_length=20, choices=STATUSES,
        default="Waiting", verbose_name="Статус выполнения"
        )
    belong = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name="Создатель")

from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.views.generic import ListView, DetailView, CreateView
from django.views import View
from django.contrib.auth.models import User, Group
from todolist.forms import UserForm
from todolist.models import Task
from django.urls import reverse_lazy
from django.contrib.auth.models import User



class TaskList(ListView):
    model = Task

    template_name = "todolist/tasks.html"

    ordering = ['status', 'priority']

    def get_ordering(self):
        ordering = self.request.GET.get('ordering', 'priority')
        return ordering

    def get_queryset(self):
        order = self.get_ordering()
        user = self.request.user
        if self.is_admin():
            self.user_tasks = Task.objects.all().order_by(order)
        else:
            self.user_tasks = Task.objects.filter(belong=user).order_by(order)
        return self.user_tasks

    def is_admin(self):
        admin_group = Group.objects.get(name="Admin")
        if admin_group in self.request.user.groups.all():
            return True
        return False

class TaskView(DetailView):
    model = Task

    template_name = "todolist/task.html"

class CreateTask(CreateView):
    model = Task
    fields = ['title', 'description', 'priority', 'status']
    template_name = 'todolist/create.html'
    success_url = '/'

    def form_valid(self, form):
        form.instance.belong = self.request.user
        return super(CreateTask, self).form_valid(form)
    


def sign_up(request):
    user_form = UserForm()
    info = None

    if request.method == "POST":
        user = UserForm(request.POST)

        if user.is_valid():
            User.objects.create_user(**user.cleaned_data)
            info = "User created"
        else:
            info = user.errors

    return render(request, 'todolist/sign_up.html', {
        'user_form':user_form,
        'info':info
    })
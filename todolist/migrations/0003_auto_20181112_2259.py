# Generated by Django 2.0.3 on 2018-11-12 15:59

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('todolist', '0002_auto_20181111_1254'),
    ]

    operations = [
        migrations.AlterField(
            model_name='task',
            name='belong',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL, verbose_name='Создатель'),
        ),
    ]
